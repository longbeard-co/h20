var $ = jQuery.noConflict();

/*********************
ANONYMOUS FUNCTIONS
*********************/
$(document).ready(function() {
    // I'll take all the help I can get 
    console.log("✝JMJ");
    
    console.log('test');

    lb_menu();
    lb_tabs();
    moreSaintsText();
    lb_newsletter();
    lb_smooth_scroll();
    lb_participate();
    pressClickOff();

    // Business Ethics
    lb_timeline_tabs('#ct_code_block_105_post_333');
    // Squared Roots
    lb_timeline_tabs('#ct_code_block_73_post_331');

    if ($('body').hasClass('page-id-329')) {
        about_slider();
    }

    // media_sliders();
    media_sliders_videos();
    forum_slider();
    panelistSlider();
    addTarget();

    // initiatives
    lb_timeline_tabs('#ct_code_block_229_post_626');
    if ($('.ct-section').hasClass('well-being-media-section')) {
        lb_media_tabs('#ct_code_block_223_post_1615');
    }

    if (screen.width < 2000) {
        truncateText('#ct_section_264_post_1615 article:not(:first-child) .content');
    }

    if (screen.width < 999) {
        readMore($('.read-more-initiatives'), 1, 9999);
    }
    else {
        readMore($('.read-more-initiatives'), 20, 9999);
    }

    // if (screen.width < 600) {
    //     readMore($('.read-more-well-being'), 4, 9999);
    // }
    // else if (screen.width < 2000) {
    //     readMore($('.read-more-well-being'), 8, 9999);
    // }

    if (screen.width < 2000) {
        readMorePress($('.post-content .press'), 0, 9999);
    }
    
        if ((screen.width < 999) && (screen.width > 600) && $('body').hasClass('page-id-1615')) {
        readMore($('.read-more-well-being'), 5, 9999);
    }

});


window.sr = ScrollReveal();
sr.reveal('.sr-opacity', {
    scale: 1,
    origin: 'bottom',
    mobile: false,
    distance: '0px',
    delay: 350,
    duration: 650
});

sr.reveal('.sr-fade-op', {
    scale: 1,
    origin: 'bottom',
    mobile: false,
    distance: '10px',
    delay: 150,
    duration: 650
});

sr.reveal('.hero-load', {
    duration: 2000,
    scale: 1,
    delay: 300,
    origin: 'bottom',
    distance: '20px',
    mobile: false,

}, 50);

/*********************
DECLARED FUNCTIONS
*********************/

function about_slider() {
    var bodySlider = $('#ct_code_block_70_post_329 .slider')
    var bodyNav = $('#ct_code_block_70_post_329 .timeline ul')
    var navCount = $('#ct_code_block_70_post_329 .timeline ul li')

    $(bodySlider).slick({
        dots: false,
        speed: 500,
        fade: true,
        arrows: false,
        cssEase: 'linear',
        adaptiveHeight: true
    });

    $(bodyNav).slick({
        slidesToShow: navCount.length,
        slidesToScroll: 1,
        asNavFor: bodySlider,
        dots: false,
        arrows: false,
        focusOnSelect: true
    });
}

function lb_media_tabs(element) {
    function slickInit(ele) {
        $(ele).find('.tab-bodies > .tab-body').each(function() {
            var bodySlider = $(this).find('.information');
            var bodyNav = $(this).find('.timeline');
            var navCount = $(this).find('.timeline li')

            $(bodySlider).slick({
                slidesToShow: 1,
                dots: false,
                speed: 300,
                fade: true,
                // centerMode: true,
                arrows: false,
                cssEase: 'linear',
                variableWidth: true,
            });

            $(bodyNav).slick({
                slidesToShow: navCount.length,
                slidesToScroll: 1,
                asNavFor: bodySlider,
                dots: false,
                arrows: false,
                focusOnSelect: true,
            });
            $(this).addClass('ready');
        });
    }

    slickInit(element);

    $('.tab-nav li a').click(function(e) {
        e.preventDefault();

        $('.tab-nav li a').each(function() {
            $(this).removeClass('active');
        });

        $(this).addClass('active');

        var mediaType = $(this).attr('href');

        $('.tab-bodies .tab-body').each(function() {
            $(this).removeClass('active');
        });

        var tabNeeded = $('.tab-bodies').find('.media-' + mediaType.slice(1) + '');

        $(tabNeeded).addClass('active');
        // console.log(tabNeeded);
        $(tabNeeded).find('.information').slick('reinit');
    });

    $(element).find('.tab-nav ul li:first-child a').click();
}

function lb_timeline_tabs(element) {
    function slickInit(ele) {
        $(ele).find('.tab-bodies > .tab-body').each(function() {
            var bodySlider = $(this).find('.information');
            var bodyNav = $(this).find('.timeline');
            var navCount = $(this).find('.timeline li')

            $(bodySlider).slick({
                slidesToShow: 1,
                dots: false,
                speed: 300,
                fade: true,
                // centerMode: true,
                arrows: false,
                cssEase: 'linear',
                variableWidth: true,
            });

            $(bodyNav).slick({
                slidesToShow: navCount.length,
                slidesToScroll: 1,
                asNavFor: bodySlider,
                dots: false,
                arrows: false,
                focusOnSelect: true,
            });

            $(this).addClass('ready');
        });
    }

    slickInit(element);

    $('.tab-nav li a').click(function(e) {
        e.preventDefault();

        $('.tab-nav li a').each(function() {
            $(this).removeClass('active');
        });

        $(this).addClass('active');

        var year = $(this).attr('href');

        $('.tab-bodies .tab-body').each(function() {
            $(this).removeClass('active');
        });

        var tabNeeded = $('.tab-bodies').find('.year-' + year.slice(1) + '');

        $(tabNeeded).addClass('active');

        $(tabNeeded).find('.information').slick('reinit');
    });

    $(element).find('.tab-nav ul li:first-child a').click();
}

function lb_menu() {

    $(window).click(function() {
        //Hide the menus if visible
        if ($('.lb-header').hasClass('active') === true) {
            $('.lb-header').removeClass('active');
        }
    });

    // $('.lb-menu-button').hover(function(){
    //     $(this).toggleClass("hovered");
    //     console.log("hovered");
    // });

    // mobile menu button activation
    $('.lb-menu-button').click(function(e) {
        $('.lb-header').toggleClass('active');
    });

    // stop propgation
    $('.lb-header').click(function(event) {
        event.stopPropagation();
    });

    // Sub menu

    $('li.menu-item-has-children').click(function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('.sub-menu').slideUp();
        }
        
        else {
            $(this).addClass('active');
            $(this).find('.sub-menu').slideDown();
        }
        
        // $('li').removeClass('active');
        // $(this).toggleClass('active');
        // $('.sub-menu').slideUp();

        // if ($('li').hasClass('active')) {
        //     $(this).find('.sub-menu').slideDown();
        // };

    })
}

function moreSaintsText() {
    $('.more-text').each(function() {
        var parentDiv = $(this).closest('.lb-bio');

        parentDiv.after('<div class="more-wrapper"><span>Read More</span><svg class="read-more-button ct-Edinburghicon-Chevron ct-svg-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Edinburghicon-Chevron"><svg id="Edinburghicon-Chevron" viewBox="0 0 45 24" width="100%" height="100%"><title>Chevron</title><path d="M0.939 0.939l22.206 22.206z"></path><path d="M0.077 1.781l1.812-1.812 22.225 22.225-1.812 1.812-22.225-22.225z"></path><path d="M43.644 0.939l-22.206 22.206z"></path><path d="M20.575 22.194l22.225-22.225 1.812 1.812-22.225 22.225-1.812-1.812z"></path></svg></use></svg></div>');

        $('.more-wrapper').click(function() {
            $(this).prev('.lb-bio').children('.more-text').slideDown();
            $(this).slideUp();
        });
    });
}

// simple tabs
function lb_tabs() {
    var lbButton = $('.lb-row-participate .lb-links a');

    $(lbButton).first().addClass("active");

    // click function
    $(lbButton).click(function() {
        var lbHref = $(this).attr('href').substr(1);

        // remove active class
        $(lbButton).each(function() {
            $(this).removeClass('active');
        });
        // add to link clicked
        $(this).addClass('active');

        // remove active class
        $('.lb-tab-body').each(function() {
            $(this).removeClass('active');
        });
        // add to body relative to link clicked
        $('.lb-tab-body.' + lbHref).addClass('active');
    });
}


function lb_participate() {

    $(window).click(function() {
        //Hide the menus if visible
        if ($('body').hasClass('popup-open') === true) {

            $('.form-popup').fadeOut(350);
            $('body').removeClass('popup-open');
        }
    });

    // stop propgation
    $('[data-popup-open]').click(function(event) {
        event.stopPropagation();

    });

    // stop propgation
    $('.form-popup-inner').click(function(event) {
        event.stopPropagation();

    });


    $(function() {
        //----- OPEN
        $('[data-popup-open]').on('click', function(e) {

            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            $("body").addClass("popup-open");
            e.preventDefault();


        });

        //----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            $("body").removeClass("popup-open");

            e.preventDefault();
        });
    });


}


// newsletter pop on hero
function lb_newsletter() {
    // console.log('storage');

    if (localStorage.getItem("visitedState") != "visited") {

        localStorage.setItem("visitedState", "visited");
        $('.lb-hero-newsletter').addClass('active');
    }
}


function lb_smooth_scroll() {
    // Select all links with hashes
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function() {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        }
                        else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });
}

function media_sliders() {
    $('.photos-inner-wrap').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<svg class="prev-arrow" width="18" height="27" viewBox="0 0 18 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M17 25.8024L13.3939 22.6617L9.78985 19.5212L2.43289 13.1128L17 0.930636" stroke="black" stroke-width="2"/></svg>',
        nextArrow: '<svg class="next-arrow" width="18" height="27" viewBox="0 0 18 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 25.8024L4.60614 22.6617L8.21015 19.5212L15.5671 13.1128L1 0.930636" stroke="black" stroke-width="2"/></svg>',
        centerMode: true,
        infinite: true,
        responsive: [{
                breakpoint: 9999,
                settings: "unslick",
            },

            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,
                    centerMode: true,
                    centerPadding: '8.333vw',
                }
            }


        ]
    });
}


function media_sliders_videos() {
    $('#ct_section_8_post_399 .videos-inner-wrap, #ct_section_170_post_12 .videos-inner-wrap').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<svg class="prev-arrow" width="18" height="27" viewBox="0 0 18 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M17 25.8024L13.3939 22.6617L9.78985 19.5212L2.43289 13.1128L17 0.930636" stroke="black" stroke-width="2"/></svg>',
        nextArrow: '<svg class="next-arrow" width="18" height="27" viewBox="0 0 18 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 25.8024L4.60614 22.6617L8.21015 19.5212L15.5671 13.1128L1 0.930636" stroke="black" stroke-width="2"/></svg>',
        // centerMode: true,
        infinite: true,
        responsive: [

            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,

                }
            },

            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,
                    centerMode: true,
                    centerPadding: '8.333vw',
                }
            },


        ]
    });
}

function forum_slider() {
    $('.photo-section .gallery-wrapper').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        lazyLoad: 'ondemand',
        dots: false,
        arrows: true,
        prevArrow: '<svg class="prev-arrow" width="18" height="27" viewBox="0 0 18 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M17 25.8024L13.3939 22.6617L9.78985 19.5212L2.43289 13.1128L17 0.930636" stroke="black" stroke-width="2"/></svg>',
        nextArrow: '<svg class="next-arrow" width="18" height="27" viewBox="0 0 18 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 25.8024L4.60614 22.6617L8.21015 19.5212L15.5671 13.1128L1 0.930636" stroke="black" stroke-width="2"/></svg>',
        centerMode: true,
        variableWidth: true,
        centerPadding: '40px',
        infinite: true,
        responsive: [
            // {
            //     breakpoint: 1025,
            //     settings: "unslick",
            // },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,
                    centerMode: true,
                    centerPadding: '8.333vw',
                    variableWidth: false,
                }
            }
        ]
    });
}

function panelistSlider() {
  $('.panelists-row:not(.advisory-board) .panelists-items-wrapper').slick({
        infinite: true,
        speed: 400,
        autoplay: false, 
        dots: false,
        // arrows: false,
        // centerMode: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        prevArrow: '<svg class="prev-arrow" width="18" height="27" viewBox="0 0 18 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M17 25.8024L13.3939 22.6617L9.78985 19.5212L2.43289 13.1128L17 0.930636" stroke="black" stroke-width="2"/></svg>',
        nextArrow: '<svg class="next-arrow" width="18" height="27" viewBox="0 0 18 27" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 25.8024L4.60614 22.6617L8.21015 19.5212L15.5671 13.1128L1 0.930636" stroke="black" stroke-width="2"/></svg>',
        responsive: [
            {
                breakpoint: 1600, 
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 1049, 
                settings: {
                    infinite: true,
                    speed: 400,
                    autoplay: false,
                    dots: false,
                    arrows: true,
                    slidesToShow: 3,
                    centerMode: false,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600, 
                settings: 'unslick',
            }
        ],
  });
}

function addTarget() {
    $('#ct_text_block_49_post_331 > a').attr('target', '_blank');
}

/*********************
HELPER METHODS
*********************/

function readMore(jObj, lineNum, viewPort) {
    if (isNaN(lineNum)) {
        lineNum = 4;
    }

    var go = new ReadMore(jObj, lineNum, viewPort);

}

//class
function ReadMore(_jObj, lineNum, viewPort) {
    if ($(window).width() <= viewPort) {
        $(_jObj).each(function() {
            var READ_MORE_LABEL = 'Read More';
            var HIDE_LABEL = 'Read Less';

            var jObj = $(this);
            var textMinHeight = '' + (parseInt(jObj.children('.hidden-content').css('line-height'), 10) * lineNum) + 'px';
            var textMaxHeight = '' + jObj.children('.hidden-content').css('height');

            jObj.find('.hidden-content').css('height', '' + textMaxHeight);
            jObj.find('.hidden-content').css('transition', 'height .5s');
            jObj.find('.hidden-content').css('height', '' + textMinHeight);

            jObj.append('<div class="button-wrapper"><button class="read-more btn">' + READ_MORE_LABEL + '</button></div>');

            jObj.find('.read-more').click(function() {
                if (jObj.find('.hidden-content').css('height') === textMinHeight) {
                    jObj.find('.hidden-content').css('height', '' + textMaxHeight);
                    jObj.find('.read-more').html(HIDE_LABEL).addClass('active');
                }
                else {
                    jObj.find('.hidden-content').css('height', '' + textMinHeight);
                    jObj.find('.read-more').html(READ_MORE_LABEL).removeClass('active');
                }
            });
        });
    }


    function resizing() {
        if ($(window).width() <= viewPort) {
            $(_jObj).each(function() {
                var READ_MORE_LABEL = 'Read More';
                var HIDE_LABEL = 'View Less';

                var jObj = $(this);
                var textMinHeight = '' + (parseInt(jObj.children('.hidden-content').css('line-height'), 10) * lineNum) + 'px';
                var textMaxHeight = '' + jObj.children('.hidden-content')[0].scrollHeight;

                jObj.find('.hidden-content').css('height', '' + textMaxHeight);
                jObj.find('.hidden-content').css('transition', 'height .5s');
                jObj.find('.hidden-content').css('height', '' + textMinHeight);

                if (!jObj.find('.read-more').length > 0) {
                    jObj.append('<button class="read-more btn">' + READ_MORE_LABEL + '</button>');
                }

                jObj.find('.read-more').click(function() {
                    if (jObj.find('.hidden-content').css('height') === textMinHeight) {
                        jObj.find('.hidden-content').css('height', '' + textMaxHeight);
                        jObj.find('.read-more').html(HIDE_LABEL).addClass('active');
                    }
                    else {
                        jObj.find('.hidden-content').css('height', '' + textMinHeight);
                        jObj.find('.read-more').html(READ_MORE_LABEL).removeClass('active');
                    }
                });
            });

        }
        else {
            $(_jObj).each(function() {
                var jObj = $(this);

                jObj.find('.hidden-content').css('height', '');
                jObj.find('button').remove();
            });
        }
    }

    $(window).resize(resizing);

}

function readMorePress(jObj, lineNum, viewPort) {
    if (isNaN(lineNum)) {
        lineNum = 4;
    }

    var go = new ReadMorePress(jObj, lineNum, viewPort);

}

function ReadMorePress(_jObj, lineNum, viewPort) {
    if ($(window).width() <= viewPort) {
        $(_jObj).each(function() {
            var READ_MORE_LABEL = 'Press';
            var HIDE_LABEL = 'Press';

            var jObj = $(this);
            var textMinHeight = '' + (parseInt(jObj.children('.hidden-content').css('line-height'), 10) * lineNum) + 'px';
            var textMaxHeight = '' + jObj.children('.hidden-content').css('height');

            jObj.find('.hidden-content').css('height', '' + textMaxHeight);
            jObj.find('.hidden-content').css('transition', 'height .5s');
            jObj.find('.hidden-content').css('height', '' + textMinHeight);

            jObj.prepend('<div class="button-container"><button class="read-more btn">' + READ_MORE_LABEL + '</button></div>');

            jObj.find('.read-more').click(function() {
                if (jObj.find('.hidden-content').css('height') === textMinHeight) {
                    jObj.find('.hidden-content').css('height', '' + textMaxHeight);
                    jObj.find('.read-more').html(HIDE_LABEL).addClass('active');
                }
                else {
                    jObj.find('.hidden-content').css('height', '' + textMinHeight);
                    jObj.find('.read-more').html(READ_MORE_LABEL).removeClass('active');
                }
            });
        });
    }


    function resizing() {
        if ($(window).width() <= viewPort) {
            $(_jObj).each(function() {
                var READ_MORE_LABEL = 'Press';
                var HIDE_LABEL = 'Press';

                var jObj = $(this);
                var textMinHeight = '' + (parseInt(jObj.children('.hidden-content').css('line-height'), 10) * lineNum) + 'px';
                var textMaxHeight = '' + jObj.children('.hidden-content')[0].scrollHeight;

                jObj.find('.hidden-content').css('height', '' + textMaxHeight);
                jObj.find('.hidden-content').css('transition', 'height .5s');
                jObj.find('.hidden-content').css('height', '' + textMinHeight);

                if (!jObj.find('.read-more').length > 0) {
                    jObj.prepend('<button class="read-more btn">' + READ_MORE_LABEL + '</button>');
                }

                jObj.find('.read-more').click(function() {
                    if (jObj.find('.hidden-content').css('height') === textMinHeight) {
                        jObj.find('.hidden-content').css('height', '' + textMaxHeight);
                        jObj.find('.read-more').html(HIDE_LABEL).addClass('active');
                    }
                    else {
                        jObj.find('.hidden-content').css('height', '' + textMinHeight);
                        jObj.find('.read-more').html(READ_MORE_LABEL).removeClass('active');
                    }
                });
            });

        }
        else {
            $(_jObj).each(function() {
                var jObj = $(this);

                jObj.find('.hidden-content').css('height', '');
                jObj.find('button').remove();
            });
        }
    }

    $(window).resize(resizing);

}

function truncateText(text) {
    var text = $(text);

    if (!text.length) {
        return;
    }

    text.each(function() {
        $(this).addClass('truncate-ready');

        if (!$(this).data('original-text')) {
            var originalText = $(this).text();
            $(this).attr('data-original-text', originalText);
        }
    })

    // $(window).on('load resize orientationchange', function() {
    //     text.each(function() {
    //         truncate($(this));
    //     });
    // })

    $(window).on('load', function() {
        text.each(function() {
            truncate($(this));
        });
    })

    function truncate(ele) {
        // if ( ele.hasClass('truncate-expanded') ) return;

        var inner = ele.find('>p');

        inner.text(ele.data('original-text'));

        var lineHeight = parseFloat(inner.css('lineHeight'));
        var outerHeight = inner.outerHeight();
        var lines = outerHeight / lineHeight;

        if (lines > 3) {
            var maxHeight = lineHeight * 3;
            var curHeight = ele.height();

            ele.height(curHeight).animate({ height: maxHeight },
                500,
                function() {
                    ele
                        .css({
                            'max-height': maxHeight,

                            'position': 'relative'
                        })
                        .addClass('truncate-initialized');

                    while (inner.outerHeight() > maxHeight) {
                        inner.text(function(index, text) {
                            return text.replace(/\W*\s(\S)*$/, '');
                        });
                    }
                    counter = 0;

                    while (counter < 3) {
                        inner.text(function(index, text) {
                            return text.replace(/\W*\s(\S)*$/, '');
                        });
                        counter++;
                    }
                    inner.text(function(index, text) {
                        return text.replace(/\W*\s(\S)*$/, '...');
                    }); // delete one more word

                    inner.append('&nbsp;<a class="read-more-inline" href="#!">Read More</a>');

                    $('.read-more-inline').click(function(e) {
                        e.preventDefault();

                        var parent = $(this).closest(text);
                        var parentInner = parent.find('> p');
                        var curHeight = parent.height();
                        parent.css({
                            'height': curHeight,
                            'max-height': 'initial'
                        });

                        parentInner
                            .text(parent.data('original-text'))
                            .append('&nbsp;<a class="read-more-inline close" href="#!">Read Less</a>');

                        var autoHeight = parentInner.css('height', 'auto').height();

                        parent
                            .height(curHeight).animate({ height: autoHeight }, 500, function() { parent.height('auto'); })
                            .addClass('truncate-expanded');

                        $(this).fadeOut(500, function() {
                            $(this).detach();
                        })

                        $('.read-more-inline.close').click(function(e) {
                            e.preventDefault();
                            truncate($(this).closest('.content'));
                            $(window).on('resize', function() {
                                truncate($(this).closest('.content'));
                            })
                        });
                    })
                });

        }
        else {
            if (ele.hasClass('truncate-initialized')) {
                ele.removeClass('truncate-initialized');
                ele.find('.read-more').detach();
                inner.text(ele.data('original-text'));
            }
        }
    }
}

function pressClickOff() {
    $('body').click(function() {
        $("#ct_section_264_post_1615 .ct-section-inner-wrap .ct-code-block article .post-content .press .button-container .btn").click(function(e) {
            e.stopPropagation();
        });
        $("#ct_section_264_post_1615 .ct-section-inner-wrap .ct-code-block article .post-content .press .hidden-content").click(function(e) {
            e.stopPropagation();
        });
        $("#ct_section_264_post_1615 .ct-section-inner-wrap .ct-code-block article .post-content .press .button-container .btn.active").trigger('click');
    });
}