var $ = jQuery.noConflict();

/*********************
ANONYMOUS FUNCTIONS
*********************/
$(document).ready(function() {
    // I'll take all the help I can get 
    console.log("✝JMJ");
    
    lb_menu();
    lb_tabs();
    moreSaintsText();
    lb_newsletter();
    lb_smooth_scroll();
    lb_participate();
});


window.sr = ScrollReveal();
sr.reveal('.sr-opacity', {
    scale: 1,
    origin: 'bottom',
    mobile: false,
    distance: '0px',
    delay: 350,
    duration: 650
});

sr.reveal('.sr-fade-op', {
    scale: 1,
    origin: 'bottom',
    mobile: false,
    distance: '10px',
    delay: 150,
    duration: 650
});

sr.reveal('.hero-load', { 
    duration: 2000,
    scale: 1,
    delay: 300,
    origin: 'bottom',
    distance: '20px',
    mobile: false,

}, 50);

/*********************
DECLARED FUNCTIONS
*********************/

function lb_menu() {

    $(window).click(function() {
        //Hide the menus if visible
        if ($('.lb-header').hasClass('active') === true) {
            $('.lb-header').removeClass('active');
        }
    });

    // mobile menu button activation
    $('.lb-menu-button').click(function(e) {
        $('.lb-header').toggleClass('active');
        // console.log("no");
    });

    // stop propgation
    $('.lb-header').click(function(event) {
        event.stopPropagation();
    });
}

function moreSaintsText() {
    $('.more-text').each(function() {
        var parentDiv = $(this).closest('.lb-bio');

        parentDiv.after('<div class="more-wrapper"><span>Read More</span><svg class="read-more-button ct-Edinburghicon-Chevron ct-svg-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Edinburghicon-Chevron"><svg id="Edinburghicon-Chevron" viewBox="0 0 45 24" width="100%" height="100%"><title>Chevron</title><path d="M0.939 0.939l22.206 22.206z"></path><path d="M0.077 1.781l1.812-1.812 22.225 22.225-1.812 1.812-22.225-22.225z"></path><path d="M43.644 0.939l-22.206 22.206z"></path><path d="M20.575 22.194l22.225-22.225 1.812 1.812-22.225 22.225-1.812-1.812z"></path></svg></use></svg></div>');

        $('.more-wrapper').click(function() {
            $(this).prev('.lb-bio').children('.more-text').slideDown();
            $(this).slideUp();
        });
    });
}

// simple tabs
function lb_tabs() {
    var lbButton = $('.lb-row-participate .lb-links a');

    $(lbButton).first().addClass("active");

    // click function
    $(lbButton).click(function() {
        var lbHref = $(this).attr('href').substr(1);
        // console.log("yes");

        // remove active class
        $(lbButton).each(function() {
            $(this).removeClass('active');
        });
        // add to link clicked
        $(this).addClass('active');

        // remove active class
        $('.lb-tab-body').each(function() {
            $(this).removeClass('active');
        });
        // add to body relative to link clicked
        $('.lb-tab-body.' + lbHref).addClass('active');
    });
}


function lb_participate() {
    $(function() {
        //----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            // console.log("Open!");
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            $("body").addClass("popup-open");
            e.preventDefault();
        });

        //----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            $("body").removeClass("popup-open");

            e.preventDefault();
        });
    });
}


// newsletter pop on hero
function lb_newsletter() {
    // console.log('storage');

    if (localStorage.getItem("visitedState") != "visited") {
        // console.log('nope');
        localStorage.setItem("visitedState", "visited");
        $('.lb-hero-newsletter').addClass('active');
    }
}


function lb_smooth_scroll() {
    // Select all links with hashes
    $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function() {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });
}

/*********************
HELPER METHODS
*********************/