// Vars

const gaTrackingID = 'UA-67479109-27';
const privacyPolicyURL = '/privacy-policy/';
const cookiePolicyURL = '/cookie-policy/';

const wildcardDomain = location.hostname.includes('longbeardco.com') ? '.longbeardco.com' : '.' + location.hostname;
const blockingCookies = [
	{
		name: '_gid',
		domain: wildcardDomain,
	},
	{ 
		name: '_ga',
		domain: wildcardDomain,
	},
	{ 
		name: '_gat_gtag_' + gaTrackingID.replace(/-/g, '_'),
		domain: wildcardDomain, 	
	}
];


// Initialize CookieConsent

window.addEventListener('load', function(){
	cookieconsent.initialise({
		// palette: {
		// 	popup: {
		// 		background: '#f3f3f3',
		// 		text: '#454545;'
		// 	},
		// 	button: {
		// 		background: '#9A6D32',
		// 		text: '#ffffff',
		// 	},
		// 	highlight: {
		// 		background: '#9A6D32',
		// 		text: '#ffffff',
		// 	}
		// },
		type: 'opt-in',
		revokable: true,
		showLink: false,
		content: {
			message: 'By using this website, you agree to our <a href="' + privacyPolicyURL + '" target="_blank">Privacy Policy</a> and <a href="' + cookiePolicyURL + '" target="_blank">Cookie Policy.</a>',
			dismiss: 'Accept',
			allow: 'Accept',
			deny: 'Reject',
		},
		
		onStatusChange: function (status, chosenBefore) {
			if (status === 'allow') 
    			gtag('consent', 'default', {
                    'ad_storage': 'granted',
                    'analytics_storage': 'granted',
                    'wait_for_update': 500
                  });
			if (status === 'deny') 
    			gtag('consent', 'update', {
                    'ad_storage': 'denied',
                    'analytics_storage': 'denied',
                    'wait_for_update': 500
                  });
		},
		onRevokeChoice: function() {
            removeCookies(blockingCookies);
		}
	})
});


function removeCookies(cookies, scripts) {

	// Delete Cookies
	cookies.map(function(c) {
		const cookieName = c.name;
		const cookieDomain = c.domain ? c.domain : '';
		deleteCookie(cookieName, cookieDomain);
	})
	
}

function deleteCookie(name,domain) {
	document.cookie = name + '=; domain=' + domain + ';expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
}

function revokeCC(e) {
	if ( e ) e.preventDefault();
	document.getElementsByClassName('cc-revoke')[0].click();
}