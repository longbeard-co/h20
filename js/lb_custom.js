var $ = jQuery.noConflict();

/*********************
ANONYMOUS FUNCTIONS
*********************/
$(document).ready(function() {
    // I'll take all the help I can get 
    console.log("✝JMJ");

    lb_menu();
    lb_tabs();
    moreSaintsText();
    lb_newsletter();
    lb_smooth_scroll();
    lb_participate();

    // Business Ethics
     lb_timeline_tabs('#ct_code_block_105_post_333'); 
    // Squared Roots
    lb_timeline_tabs('#ct_code_block_73_post_331');
if ( $('body').hasClass('page-id-329') ) {
    about_slider();
}

    media_sliders(); 
    media_sliders_videos(); 
    forum_slider();
    addTarget();

    // initiatives
     lb_timeline_tabs('#ct_code_block_229_post_626'); 
});


window.sr = ScrollReveal();
sr.reveal('.sr-opacity', {
    scale: 1,
    origin: 'bottom',
    mobile: false,
    distance: '0px',
    delay: 350,
    duration: 650 
});

sr.reveal('.sr-fade-op', {
    scale: 1,
    origin: 'bottom',
    mobile: false,
    distance: '10px',
    delay: 150,
    duration: 650
});

sr.reveal('.hero-load', {
    duration: 2000,
    scale: 1,
    delay: 300,
    origin: 'bottom',
    distance: '20px',
    mobile: false,

}, 50);

/*********************
DECLARED FUNCTIONS
*********************/

function about_slider() {
    var bodySlider = $('#ct_code_block_70_post_329 .slider')
    var bodyNav = $('#ct_code_block_70_post_329 .timeline ul')
    var navCount = $('#ct_code_block_70_post_329 .timeline ul li')

    $(bodySlider).slick({
      dots: false,
      speed: 500,
      fade: true,
      arrows: false,
      cssEase: 'linear',
    });

    $(bodyNav).slick({
      slidesToShow: navCount.length,
      slidesToScroll: 1,
      asNavFor: bodySlider,
      dots: false,
      arrows: false,
      focusOnSelect: true
    });
}

function lb_timeline_tabs(element) {
    function slickInit(ele) {
        $(ele).find('.tab-bodies > .tab-body').each(function() {
            var bodySlider = $(this).find('.information');
            var bodyNav = $(this).find('.timeline');
            var navCount = $(this).find('.timeline li')

            $(bodySlider).slick({
              slidesToShow: 1,
              dots: false,
              speed: 300,
               fade: true,
            // centerMode: true,
              arrows: false,
              cssEase: 'linear',
              variableWidth: true,
            });

            $(bodyNav).slick({
              slidesToShow: navCount.length,
              slidesToScroll: 1,
              asNavFor: bodySlider,
              dots: false,
              arrows: false,
              focusOnSelect: true,
            }); 

            $(this).addClass('ready');   
        });
    }

    slickInit(element); 

    $('.tab-nav li a').click(function(e){
        e.preventDefault();

        $('.tab-nav li a').each(function(){
            $(this).removeClass('active'); 
        });

        $(this).addClass('active');

        var year = $(this).attr('href');

        $('.tab-bodies .tab-body').each(function(){
            $(this).removeClass('active'); 
        });

        var tabNeeded = $('.tab-bodies').find('.year-'+year.slice(1)+'');

        $(tabNeeded).addClass('active');
        console.log(tabNeeded);
        $(tabNeeded).find('.information').slick('reinit');
    });

    $(element).find('.tab-nav ul li:first-child a').click();
}

function lb_menu() { 

    $(window).click(function() {
        //Hide the menus if visible
        if ($('.lb-header').hasClass('active') === true) {
            $('.lb-header').removeClass('active');
        }
    });

    // $('.lb-menu-button').hover(function(){
    //     $(this).toggleClass("hovered");
    //     console.log("hovered");
    // });

    // mobile menu button activation
    $('.lb-menu-button').click(function(e) {
        $('.lb-header').toggleClass('active');
        // console.log("no");
    });

    // stop propgation
    $('.lb-header').click(function(event) {
        event.stopPropagation();
    });

    // Sub menu

    $('li.menu-item-has-children').click(function() {
        $('li').removeClass('active');
        $('.sub-menu').slideUp();
        $(this).toggleClass('active');

        if( $('li').hasClass('active') ) {
            $(this).find('.sub-menu').slideDown();
        };

    })
}

function moreSaintsText() {
    $('.more-text').each(function() {
        var parentDiv = $(this).closest('.lb-bio');

        parentDiv.after('<div class="more-wrapper"><span>Read More</span><svg class="read-more-button ct-Edinburghicon-Chevron ct-svg-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Edinburghicon-Chevron"><svg id="Edinburghicon-Chevron" viewBox="0 0 45 24" width="100%" height="100%"><title>Chevron</title><path d="M0.939 0.939l22.206 22.206z"></path><path d="M0.077 1.781l1.812-1.812 22.225 22.225-1.812 1.812-22.225-22.225z"></path><path d="M43.644 0.939l-22.206 22.206z"></path><path d="M20.575 22.194l22.225-22.225 1.812 1.812-22.225 22.225-1.812-1.812z"></path></svg></use></svg></div>');

        $('.more-wrapper').click(function() {
            $(this).prev('.lb-bio').children('.more-text').slideDown();
            $(this).slideUp();
        });
    });
}

// simple tabs
function lb_tabs() {
    var lbButton = $('.lb-row-participate .lb-links a');

    $(lbButton).first().addClass("active");

    // click function
    $(lbButton).click(function() {
        var lbHref = $(this).attr('href').substr(1);
        // console.log("yes");

        // remove active class
        $(lbButton).each(function() {
            $(this).removeClass('active');
        });
        // add to link clicked
        $(this).addClass('active');

        // remove active class
        $('.lb-tab-body').each(function() {
            $(this).removeClass('active');
        });
        // add to body relative to link clicked
        $('.lb-tab-body.' + lbHref).addClass('active');
    });
}


function lb_participate() {

    $(window).click(function() {
        //Hide the menus if visible
        if ( $('body').hasClass('popup-open') === true ) {
            console.log("fade away");
            $('.form-popup').fadeOut(350);
            $('body').removeClass('popup-open');
        }
    });

    // stop propgation
    $('[data-popup-open]').click(function(event) {
        event.stopPropagation();

    });

    // stop propgation
    $('.form-popup-inner').click(function(event) {
        event.stopPropagation();

    });


    $(function() {
        //----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            // console.log("Open!");
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            $("body").addClass("popup-open");
            e.preventDefault();


        });

        //----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            $("body").removeClass("popup-open");

            e.preventDefault();
        });
    });

    
}


// newsletter pop on hero
function lb_newsletter() {
    // console.log('storage');

    if (localStorage.getItem("visitedState") != "visited") {
        // console.log('nope');
        localStorage.setItem("visitedState", "visited");
        $('.lb-hero-newsletter').addClass('active');
    }
}


function lb_smooth_scroll() {
    // Select all links with hashes
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function() {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });
}

function media_sliders() {
    $('.photos-inner-wrap').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<svg class="prev-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7.721 17.435"><defs><style>.a{fill:none;stroke:#fff;}</style></defs><path class="a" d="M12879,2007.5l1.655,2.123,1.655,2.123,3.377,4.332-6.687,8.235" transform="translate(12886.326 2024.628) rotate(180)"/></svg>',
        nextArrow: '<svg class="next-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7.721 17.435"><defs><style>.a{fill:none;stroke:#fff;}</style></defs><path class="a" d="M12879,2007.5l1.655,2.123,1.655,2.123,3.377,4.332-6.687,8.235" transform="translate(-12878.605 -2007.193)"/></svg>',
        centerMode: true,
        infinite: true,
        responsive: [
            {
                breakpoint: 9999,
                settings: "unslick",
            },

            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,
                    centerMode: true,

                }
            }


        ]
    }); 
}


function media_sliders_videos() {
    $('#ct_section_8_post_399 .videos-inner-wrap, #ct_section_170_post_12 .videos-inner-wrap').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<svg class="prev-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7.721 17.435"><defs><style>.a{fill:none;stroke:#000;}</style></defs><path class="a" d="M12879,2007.5l1.655,2.123,1.655,2.123,3.377,4.332-6.687,8.235" transform="translate(12886.326 2024.628) rotate(180)"/></svg>',
        nextArrow: '<svg class="next-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7.721 17.435"><defs><style>.a{fill:none;stroke:#000;}</style></defs><path class="a" d="M12879,2007.5l1.655,2.123,1.655,2.123,3.377,4.332-6.687,8.235" transform="translate(-12878.605 -2007.193)"/></svg>',
        // centerMode: true,
        infinite: true,
        responsive: [

            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,

                }
            },

            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,

                }
            },
            
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,
                    centerMode: true,

                }
            },


        ]
    }); 
}

function forum_slider() {
    $('.photo-section .gallery-wrapper').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<svg class="prev-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7.721 17.435"><defs><style>.a{fill:none;stroke:#000;}</style></defs><path class="a" d="M12879,2007.5l1.655,2.123,1.655,2.123,3.377,4.332-6.687,8.235" transform="translate(12886.326 2024.628) rotate(180)"/></svg>',
        nextArrow: '<svg class="next-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7.721 17.435"><defs><style>.a{fill:none;stroke:#000;}</style></defs><path class="a" d="M12879,2007.5l1.655,2.123,1.655,2.123,3.377,4.332-6.687,8.235" transform="translate(-12878.605 -2007.193)"/></svg>',
        centerMode: true,
        variableWidth: true,
        centerPadding: '40px',
        infinite: true,
        responsive: [
            // {
            //     breakpoint: 1025,
            //     settings: "unslick",
            // },
            {
                breakpoint: 600,
                settings: { 
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,
                    centerMode: true,
                    variableWidth: false,
                }
            }
        ]
    });
}

function addTarget() {
    $('#ct_text_block_49_post_331 > a').attr('target','_blank');
}

/*********************
HELPER METHODS
*********************/