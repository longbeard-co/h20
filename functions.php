<?php

require_once plugin_dir_path(__FILE__) . '/inc/wptt-webfont-loader.php';

// enqueue the child theme stylesheet

function wp_h2_enqueue_scripts() {
  // Web font
  // Load the webfont.
  wp_enqueue_style(
    'google-fonts',
    wptt_get_webfont_url('https://fonts.googleapis.com/css?family=Catamaran:400,500,600,700,800|Montserrat:400,500,600,700'),
    array(),
    '1.0'
  );

  $styleWP = plugin_dir_url(__FILE__) . 'style.css';
  $stylePath = __DIR__ . '/style.css';
  $filetime = filemtime($stylePath);

  wp_enqueue_style('child_style', $styleWP, array(), $filetime);
  wp_enqueue_script('lb_custom-js', plugin_dir_url(__FILE__) . 'js/lb_custom-js.js', array('jquery'), '1.0', true);
  wp_enqueue_script('lity', plugin_dir_url(__FILE__) . 'js/vendor/lity.min.js', array('jquery'), true);
  wp_register_style('lity-css', plugin_dir_url(__FILE__) . 'js/vendor/lity.min.css');
  wp_enqueue_style('lity-css');

  // Cookie Consent
  wp_enqueue_style('cookieconsent', plugin_dir_url(__FILE__) . '/js/vendor/cookieconsent.min.css', array(), '');
  wp_enqueue_script('cookieconsent', plugin_dir_url(__FILE__) . '/js/vendor/cookieconsent.min.js', array(), '', true);
  wp_enqueue_script('beardbalm-cookieconsent', plugin_dir_url(__FILE__) . 'js/cookieconsent.js', array(), '1.0', true);

  // Oxygen's webfont
  wp_dequeue_script('font-loader');
  wp_deregister_script('font-loader');
};

// Enqueue Google Analytics to header


function init_analytics() {
  $analyticsUA = 'UA-67479109-27';
  $analytics = '<!-- gtag.js - Set Consent Mode -->
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag(\'consent\', \'default\', {\'ad_storage\': \'denied\', \'analytics_storage\': \'denied\'});
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=' . $analyticsUA . '"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag(\'js\', new Date());

  gtag(\'config\', \'' . $analyticsUA . '\');
    
</script>';

  echo "\n" . $analytics;
}

add_action('wp_enqueue_scripts', 'init_analytics', 11);


function init_gtm() {
  $gtmCode = '<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'GTM-W63GPBB\');</script>
<!-- End Google Tag Manager -->';

  echo "\n" . $gtmCode;
}

// add_action( 'wp_enqueue_scripts', 'init_gtm', 11);


function init_favicons() {
  $favicons = '<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">';

  echo "\n" . $favicons;
}

add_action('wp_head', 'init_favicons', 10);

add_action('wp_enqueue_scripts', 'wp_h2_enqueue_scripts', 11);

add_action('init', function () {
  // Remove Oxygen default fonts to avoid loading in Google Fonts
  $oxygen_settings = get_option("ct_global_settings");
  $oxygen_settings['fonts'] = [];
  update_option('ct_global_settings', $oxygen_settings);
});

// Get custom post type
require_once('lb-custom-post-types.php');

// add lb_ classes to all applicable VC elements
// add_action( 'vc_after_init', 'add_lb_custom_class' );/* Note: here we are using vc_after_init because WPBMap::GetParam and mutateParame are available only when default content elements are "mapped" into the system */
function add_lb_custom_class() {
  //Get array of vc elements minus 'vc_' that support custom class "el_class"
  $elements = array(
    'vc_column_text'    => 'lb-column_text',
    'vc_empty_space'    => 'lb-empty_space',
    'vc_row'            => 'lb-row',
    'vc_column'         => 'lb-column',
    'vc_row_inner'      => 'lb-row_inner',
    'vc_column_inner'   => 'lb-column_inner'
  );

  //Loop over each element in array
  foreach ($elements as $oldEl => $newClass) {
    //Get current values stored in the class param in element
    $param = WPBMap::getParam($oldEl, 'el_class');
    //Append new value to the 'value' array
    $param['value'][__('el_class', $newClass)] = $newClass;
    //Finally "mutate" param with new values
    vc_update_shortcode_param($oldEl, $param);
  }
}

// // filter the Gravity Forms button type
// add_filter( 'gform_submit_button_1', 'contact_form_submit_button', 10, 2 );
// function contact_form_submit_button( $button, $form ) {
//     return "<button class='button submit-button' id='gform_submit_button_3'>Submit</button>";
// }


// filter the Gravity Forms button type
add_filter('gform_submit_button', 'form_submit_button', 10, 2);
function form_submit_button($button, $form) {
  return "<button class='button submit-button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}

/**
 * Change the base path. This is by default WP_CONTENT_DIR.
 * NOTE: Do not include trailing slash.
 */
add_filter('wptt_get_local_fonts_base_path', function ($path) {
  return plugin_dir_path(__FILE__) . '/src';
});

/**
 * Change the base URL. This is by default the content_url().
 * NOTE: Do not include trailing slash.
 */
add_filter('wptt_get_local_fonts_base_url', function ($url) {
  return plugin_dir_url(__FILE__) . '/src';
});
