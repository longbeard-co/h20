<?php 

// Register Custom Post Type
function people() {

    $labels = array(
        'name'                  => _x( 'People', 'Post Type General Name', 'humanity' ),
        'singular_name'         => _x( 'People', 'Post Type Singular Name', 'humanity' ),
        'menu_name'             => __( 'People', 'humanity' ),
        'name_admin_bar'        => __( 'People', 'humanity' ),
        'archives'              => __( 'People Archives', 'humanity' ),
        'attributes'            => __( 'People Attributes', 'humanity' ),
        'parent_item_colon'     => __( 'Parent People:', 'humanity' ),
        'all_items'             => __( 'All People', 'humanity' ),
        'add_new_item'          => __( 'Add New People', 'humanity' ),
        'add_new'               => __( 'Add New', 'humanity' ),
        'new_item'              => __( 'New People', 'humanity' ),
        'edit_item'             => __( 'Edit People', 'humanity' ),
        'update_item'           => __( 'Update People', 'humanity' ),
        'view_item'             => __( 'View People', 'humanity' ),
        'view_items'            => __( 'View People', 'humanity' ),
        'search_items'          => __( 'Search People', 'humanity' ),
        'not_found'             => __( 'Not found', 'humanity' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'humanity' ),
        'featured_image'        => __( 'Featured Image', 'humanity' ),
        'set_featured_image'    => __( 'Set featured image', 'humanity' ),
        'remove_featured_image' => __( 'Remove featured image', 'humanity' ),
        'use_featured_image'    => __( 'Use as featured image', 'humanity' ),
        'insert_into_item'      => __( 'Insert into People', 'humanity' ),
        'uploaded_to_this_item' => __( 'Uploaded to this People', 'humanity' ),
        'items_list'            => __( 'People list', 'humanity' ),
        'items_list_navigation' => __( 'People list navigation', 'humanity' ),
        'filter_items_list'     => __( 'Filter People list', 'humanity' ),
    );
    $args = array(
        'label'                 => __( 'People', 'humanity' ),
        'description'           => __( 'People part of Humanity 2.0' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'taxonomies'            => array( 'region' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 1,
        'menu_icon'             => 'dashicons-admin-users',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'people', $args );

}
add_action( 'init', 'people', 0 );



// Register Custom Post Type
function organization() {

    $labels = array(
        'name'                  => _x( 'Organizations', 'Post Type General Name', 'humanity' ),
        'singular_name'         => _x( 'Organization', 'Post Type Singular Name', 'humanity' ),
        'menu_name'             => __( 'Organizations', 'humanity' ),
        'name_admin_bar'        => __( 'Organizations', 'humanity' ),
        'archives'              => __( 'Organization Archives', 'humanity' ),
        'attributes'            => __( 'Organization Attributes', 'humanity' ),
        'parent_item_colon'     => __( 'Parent Organization:', 'humanity' ),
        'all_items'             => __( 'All Organizations', 'humanity' ),
        'add_new_item'          => __( 'Add New Organization', 'humanity' ),
        'add_new'               => __( 'Add New', 'humanity' ),
        'new_item'              => __( 'New Organization', 'humanity' ),
        'edit_item'             => __( 'Edit Organization', 'humanity' ),
        'update_item'           => __( 'Update Organization', 'humanity' ),
        'view_item'             => __( 'View Organization', 'humanity' ),
        'view_items'            => __( 'View Organizations', 'humanity' ),
        'search_items'          => __( 'Search Organizations', 'humanity' ),
        'not_found'             => __( 'Not found', 'humanity' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'humanity' ),
        'featured_image'        => __( 'Featured Image', 'humanity' ),
        'set_featured_image'    => __( 'Set featured image', 'humanity' ),
        'remove_featured_image' => __( 'Remove featured image', 'humanity' ),
        'use_featured_image'    => __( 'Use as featured image', 'humanity' ),
        'insert_into_item'      => __( 'Insert into Organization', 'humanity' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Organization', 'humanity' ),
        'items_list'            => __( 'Organization list', 'humanity' ),
        'items_list_navigation' => __( 'Organization list navigation', 'humanity' ),
        'filter_items_list'     => __( 'Filter Organization list', 'humanity' ),
    );
    $args = array(
        'label'                 => __( 'Organizations', 'humanity' ),
        'description'           => __( 'Organizations part of Humanity 2.0' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'taxonomies'            => array( 'region' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 1,
        'menu_icon'             => 'dashicons-store',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'organization', $args );

}
add_action( 'init', 'organization', 0 );
